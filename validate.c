#include "validate.h"

extern int turn;
extern int pos[8][8];
extern int option;
extern int playerColor;
int check = 0;
Move onePlayerMove;


int validate(Move move) {
	if (isPieceAvailabe(move) || isPositionAlreadyFilled(move) || isMoveValidForPiece(move))
		return 1;
	else
		return 0;
}

int hasCheck(Move move1) {
	int i = 0, j = 0;
	int kingRow = 0, kingCol = 0, lower = 0, upper = 0, toBreak = 0, piece = 0;
	Move move;
	if (!turn) {
		piece = wKING;
		lower = bPAWN;
		upper = bKING;
	} else {
		piece = bKING;
		lower = wPAWN;
		upper = wKING;
	}
	for (i = 0; i < 8; i++) {
		for (j = 0; j < 8; j++) {
			if (pos[i][j] == piece) {
				kingRow = i;
				kingCol = j;
				toBreak = 1;
				break;
			}
		}
		if (toBreak)
			break;
	}

	for (i = 0; i < 8; i++) {
		for (j = 0; j < 8; j++) {
			if (pos[i][j] >= lower && pos[i][j] <= upper) {
				move.piece = pos[i][j];
				move.row1 = i;
				move.col1 = j;
				move.row2 = kingRow;
				move.col2 = kingCol;
				if (!isPositionAlreadyFilled(move) || !isMoveValidForPiece(move)) {
					if (willHaveCheck(move1)) {
						if (option == 2 || (option == 1 && playerColor == turn))
							printf("Illegal move: Check!!\n");
						return 1;
					}
				}
			}
		}
	}
	return 0;
}

int isPinned(Move move) {
	if (willHaveCheck(move)) {
		printf("Illegal move: Piece pinned!!\n");
		return 1;
	}
	return 0;
}

int isPieceAvailabe(Move move) {
	if (pos[move.row1][move.col1] == move.piece)
		return 0;
	else
		return 1;
}

int isPositionAlreadyFilled(Move move) {
	if (!turn) {
		if (pos[move.row2][move.col2] >= wPAWN && pos[move.row2][move.col2] <= wKING)
			return 1;
		else
			return 0;
	} else {
		if (pos[move.row2][move.col2] >= bPAWN && pos[move.row2][move.col2] <= bKING)
			return 1;
		else
			return 0;
	}
	return 0;
}

int isMoveValidForPiece(Move move) {
	switch (move.piece) {
	case bPAWN:
		if ((move.row2 - move.row1 == 1 && move.col2 == move.col1) && (pos[move.row2][move.col2] == 0))
			/*
				moving straight
			*/
			return 0;
		else if (move.row2 - move.row1 == 1 && (move.col2 - move.col1 == 1 || move.col1 - move.col2 == 1) && pos[move.row2][move.col2] >= wPAWN && pos[move.row2][move.col2] <= wKING)
			/*
				capturing
			*/
			return 0;
		else if (move.row2 - move.row1 == 2 && move.col2 == move.col1 && (pos[move.row2][move.col2] == 0) && move.row1 == 1 && pos[move.row2 - 1][move.col2] == 0)
			/*
				first move can be 2 blocks ahead
			*/
			return 0;
		else
			return 1;
		break;
	case wPAWN:
		if (move.row1 - move.row2 == 1 && move.col2 == move.col1 && (pos[move.row2][move.col2] == 0))
			/*
				moving straight
			*/
			return 0;
		else if (move.row1 - move.row2 == 1 && (move.col1 - move.col2 == 1 || move.col2 - move.col1 == 1) && pos[move.row2][move.col2] >= bPAWN && pos[move.row2][move.col2] <= bKING)
			/*
				capturing
			*/
			return 0;
		else if (move.row1 - move.row2 == 2 && move.col2 == move.col1 && (pos[move.row2][move.col2] == 0) && move.row1 == 6 && pos[move.row1 - 1][move.col1] == 0)
			/*
				first move can be 2 blocks ahead
			*/
			return 0;
		else
			return 1;
		break;
	case bKNIGHT:
	case wKNIGHT:
		if (abs(move.row2 - move.row1) == 2 && abs(move.col2 - move.col1) == 1)
			/*
				vertical L
			*/
			return 0;
		else if (abs(move.col2 - move.col1) == 2 && abs(move.row2 - move.row1) == 1)
			/*
				horizontal L
			*/
			return 0;
		else
			return 1;
		break;
	case bROOK:
	case wROOK:
		if (isValidForRook(move))
			return 1;
		else
			return 0;
		break;
	case bBISHOP:
	case wBISHOP:
		if (isValidForBishop(move))
			return 1;
		else
			return 0;
		break;
	case bQUEEN:
	case wQUEEN:
		if (isValidForBishop(move) && isValidForRook(move))
			return 1;
		else
			return 0;
		break;
	case bKING:
	case wKING:
		if (move.row1 == move.row2 && move.col1 == move.col2)
			return 1;
		else if (abs(move.row1 - move.row2) == 1 || abs(move.col1 - move.col2) == 1)
			return 0;
		else
			return 1;
		break;
	}
	return 0;
}

int isValidForRook(Move move) {
	int i = 0;
	if (move.row2 == move.row1 && move.col2 != move.col1) {
		/*
			horizontal
		*/
		if (move.col1 > move.col2) {
			for (i = move.col2; i < move.col1; i++)
				if (pos[move.row1][i] != 0)
					return 1;
		} else {
			for (i = move.col1 + 1; i < move.col2; i++)
				if (pos[move.row1][i] != 0)
					return 1;
		}
		return 0;
	} else if (move.col2 == move.col1 && move.row2 != move.row1) {
		/*
			vertical
		*/
		if (move.row1 > move.row2) {
			for (i = move.row2; i < move.row1; i++)
				if (pos[i][move.col1] != 0)
					return 1;
		} else {
			for (i = move.row1 + 1; i < move.row2; i++) {
				if (pos[i][move.col1] != 0)
					return 1;
			}
		}
		return 0;
	} else {
		return 1;
	}
}


int isValidForBishop(Move move) {
	int i = 0, j = 0, k = 0, l = 0;
	if (abs(move.row1 - move.row2) == abs(move.col1 - move.col2)) {
		i = move.row1;
		j = move.row2;
		k = move.col1;
		l = move.col2;
		if (move.row1 > move.row2 && move.col1 > move.col2) {
			/*
				upper left
			*/
			for (i = i - 1, k = k - 1; i > j && k > l; i--, k--) {
				// printf("%d %d\n", i, k );
				if (pos[i][k] != 0)
					return 1;
			}
		} else if (move.row2 > move.row1 && move.col2 > move.col1) {
			/*
				lower right
			*/
			for (i = i + 1, k = k + 1; i < j && k < l; i++, k++) {
				// printf("%d %d\n", i, k );
				if (pos[i][k] != 0)
					return 1;
			}
		} else if (move.row1 > move.row2 && move.col2 > move.col1) {
			/*
				upper right
			*/
			for (i = i - 1, k = k + 1; i > j && k < l; i--, k++) {
				// printf("%d %d\n", i, k );
				if (pos[i][k] != 0)
					return 1;
			}
		} else if (move.row2 > move.row1 && move.col1 > move.col2) {
			/*
				lower left
			*/
			for (i = i + 1, k = k - 1; i > j && k < l; i++, k--) {
				// printf("%d %d\n", i, k );
				if (pos[i][k] != 0)
					return 1;
			}
		}

		// for (i = i + 1, k = k + 1; i < j && k < l; i++, k++) {
		// 	printf("%d %d\n", i, k );
		// 	if (pos[i][k] != 0)
		// 		return 1;
		// }

		return 0;
	}
	else
		return 1;
}

int willHaveCheck(Move move1) {
	int i = 0, j = 0;
	int posBck[8][8] = {0};

	//printBoard();
	//printf("3333\n");
	for (i = 0; i < 8; i++) {
		for (j = 0; j < 8; j++) {
			posBck[i][j] = pos[i][j];
		}
	}

	pos[move1.row1][move1.col1] = 0;
	pos[move1.row2][move1.col2] = move1.piece;

	int kingRow = 0, kingCol = 0, lower = 0, upper = 0, toBreak = 0, piece = 0;
	Move move;
	if (!turn) {
		piece = wKING;
		lower = bPAWN;
		upper = bKING;
	} else {
		piece = bKING;
		lower = wPAWN;
		upper = wKING;
	}
	for (i = 0; i < 8; i++) {
		for (j = 0; j < 8; j++) {
			if (pos[i][j] == piece) {
				kingRow = i;
				kingCol = j;
				toBreak = 1;
				break;
			}
		}
		if (toBreak)
			break;
	}

	for (i = 0; i < 8; i++) {
		for (j = 0; j < 8; j++) {
			if (pos[i][j] >= lower && pos[i][j] <= upper) {
				move.piece = pos[i][j];
				move.row1 = i;
				move.col1 = j;
				move.row2 = kingRow;
				move.col2 = kingCol;
				if (!isPositionAlreadyFilled(move) || !isMoveValidForPiece(move)) {
					for (i = 0; i < 8; i++) {
						for (j = 0; j < 8; j++) {
							pos[i][j] = posBck[i][j];
						}
					}
					return 1;
				}
			}
		}
	}
	for (i = 0; i < 8; ++i) {
		for (j = 0; j < 8; j++) {
			pos[i][j] = posBck[i][j];
		}
	}

	//printBoard();
	//printf("44444\n");

	return 0;
}


int isCheckMate() {
	int piece = 0, row, col, i, j, blocked = 0, checkstop = 1, toBreak = 0;

	// printBoard();
	// printf("222\n");

	Move move;
	if (!turn) {
		//white
		piece = wKING;
	} else {
		//black
		piece = bKING;
	}

	for (i = 0; i < 8; i++) {
		for (j = 0; j < 8; j++) {
			if (pos[i][j] == piece) {
				row = i;
				col = j;
				break;
			}
		}
	}

	if (isBlocked(row, col, piece))
		blocked = 1;

	for (i = 0; i < 8; i++) {
		for (j = 0; j < 8; j++) {
			if (turn) {
				if (pos[i][j] >= 7 && pos[i][j] <= 12) {
					if (!canStopCheck(pos[i][j], i, j)) {
						checkstop = 0;
						toBreak = 1;
						break;
					}
				}
			} else {
				if (pos[i][j] >= 0 && pos[i][j] <= 6) {
					if (!canStopCheck(pos[i][j], i, j)) {
						checkstop = 0;
						toBreak = 1;
						break;
					}
				}
			}
		}
		if (toBreak)
			break;
	}

	//printf("block %d checkstop %d\n", blocked, checkstop );

	// printBoard();

	// printf("----------\n");

	if (blocked && checkstop && option == 1)
		return 0;

	if (blocked && checkstop)
		return 1;

	return 0;

}

int hasPawnReachedEnd() {

	int i = 0;

	if (!turn) {

		for (i = 0; i < 7; i++) {
			if (pos[0][i] == wPAWN)
				return 1;
		}

	} else {

		for (i = 0; i < 7; i++) {
			if (pos[7][i] == bPAWN)
				return 1;
		}

	}

	return 0;
}

int isBlocked(int row, int col, int piece) {

	Move move;
	move.row1 = row;
	move.col1 = col;
	move.piece = piece;

	//printf("piece row col %d %d %d\n", piece, row, col);
	if (row != 0 && pos[row - 1][col] == 0) {
		move.row2 = row - 1;
		move.col2 = col;
		if (!willHaveCheck(move))
			return 0;
	} else if (row != 7 && pos[row + 1][col] == 0 ) {
		move.row2 = row + 1;
		move.col2 = col;
		if (!willHaveCheck(move))
			return 0;
	} else if (col != 0 && pos[row][col - 1] == 0) {
		move.row2 = row;
		move.col2 = col - 1;
		if (!willHaveCheck(move))
			return 0;
	} else if (col != 7 && pos[row][col + 1] == 0) {
		move.row2 = row;
		move.col2 = col + 1;
		if (!willHaveCheck(move))
			return 0;
	} else if (row - 1 >= 0 && col - 1 >= 0 && pos[row - 1][col - 1] == 0) {
		move.row2 = row - 1;
		move.col2 = col - 1;
		if (!willHaveCheck(move))
			return 0;
	} else if (row + 1 <= 7 && col + 1 <= 7 && pos[row + 1][col + 1] == 0) {
		move.row2 = row + 1;
		move.col2 = col + 1;
		if (!willHaveCheck(move))
			return 0;
	} else if (row - 1 >= 0 && col + 1 <= 7 && pos[row - 1][col + 1] == 0) {
		move.row2 = row - 1;
		move.col2 = col + 1;
		if (!willHaveCheck(move))
			return 0;
	} else if (row - 1 >= 0 && col - 1 >= 0 && pos[row - 1][col - 1] == 0) {
		move.row2 = row - 1;
		move.col2 = col - 1;
		if (!willHaveCheck(move))
			return 0;
	}
	return 1;
}


int canStopCheck(int piece, int row, int col) {
	Move move;
	move.piece = piece;
	move.row1 = row;
	move.col1 = col;
	//printf("canStopCheck %d %d %d\n", piece, row, col );
	switch (piece) {
	case wROOK:
	case bROOK:
		if (row != 0 && pos[row - 1][col] == 0) {
			move.row2 = row - 1;
			move.col2 = col;
			if (!willHaveCheck(move))
				return 0;
		}
		if (row != 7 && pos[row + 1][col] == 0 ) {
			move.row2 = row + 1;
			move.col2 = col;
			if (!willHaveCheck(move))
				return 0;
		}
		if (col != 0 && pos[row][col - 1] == 0) {
			move.row2 = row;
			move.col2 = col - 1;
			if (!willHaveCheck(move))
				return 0;
		}
		if (col != 7 && pos[row][col + 1] == 0) {
			move.row2 = row;
			move.col2 = col + 1;
			if (!willHaveCheck(move))
				return 0;
		}
		return 1;
		break;
	case wKING:
	case bKING:
		if (row != 0 && pos[row - 1][col] == 0) {
			move.row2 = row - 1;
			move.col2 = col;
			if (!willHaveCheck(move))
				return 0;
		}
		if (row != 7 && pos[row + 1][col] == 0 ) {
			move.row2 = row + 1;
			move.col2 = col;
			if (!willHaveCheck(move))
				return 0;
		}
		if (col != 0 && pos[row][col - 1] == 0) {
			move.row2 = row;
			move.col2 = col - 1;
			if (!willHaveCheck(move))
				return 0;
		}
		if (col != 7 && pos[row][col + 1] == 0) {
			move.row2 = row;
			move.col2 = col + 1;
			if (!willHaveCheck(move))
				return 0;
		}
		if (row - 1 >= 0 && col - 1 >= 0 && pos[row - 1][col - 1] == 0) {
			move.row2 = row - 1;
			move.col2 = col - 1;
			if (!willHaveCheck(move))
				return 0;
		}
		if (row + 1 <= 7 && col + 1 <= 7 && pos[row + 1][col + 1] == 0) {
			move.row2 = row + 1;
			move.col2 = col + 1;
			if (!willHaveCheck(move))
				return 0;
		}
		if (row - 1 >= 0 && col + 1 <= 7 && pos[row - 1][col + 1] == 0) {
			move.row2 = row - 1;
			move.col2 = col + 1;
			if (!willHaveCheck(move))
				return 0;
		}
		if (row - 1 >= 0 && col - 1 >= 0 && pos[row - 1][col - 1] == 0) {
			move.row2 = row - 1;
			move.col2 = col - 1;
			if (!willHaveCheck(move))
				return 0;
		}
		return 1;
		break;
	case wKNIGHT:
	case bKNIGHT:
		if (row - 2 >= 0 && col - 1 >= 0 && pos[row - 2][col - 1] == 0) {
			move.row2 = row - 2;
			move.col2 = col - 1;
			if (!willHaveCheck(move))
				return 0;
		}
		if (row - 2 >= 0 && col + 1 <= 7 && pos[row - 2][col + 1] == 0) {
			move.row2 = row - 2;
			move.col2 = col + 1;
			if (!willHaveCheck(move))
				return 0;
		}
		if (row + 2 <= 7 && col - 1 >= 0 && pos[row + 2][col - 1] == 0) {
			move.row2 = row + 2;
			move.col2 = col - 1;
			if (!willHaveCheck(move))
				return 0;
		}
		if (row + 2 <= 7 && col + 1 <= 7 && pos[row + 2][col + 1] == 0) {
			move.row2 = row + 2;
			move.col2 = col + 1;
			if (!willHaveCheck(move))
				return 0;
		}
		if (col - 2 >= 0 && row - 1 >= 0 && pos[row - 1][col - 2] == 0) {
			move.row2 = row - 1;
			move.col2 = col - 2;
			if (!willHaveCheck(move))
				return 0;
		}
		if (col - 2 >= 0 && row + 1 <= 7 && pos[row + 1][col - 2] == 0) {
			move.row2 = row + 1;
			move.col2 = col - 2;
			if (!willHaveCheck(move))
				return 0;
		}
		if (col + 2 <= 7 && row - 1 >= 0 && pos[row - 1][col + 2] == 0) {
			move.row2 = row - 1;
			move.col2 = col + 2;
			if (!willHaveCheck(move))
				return 0;
		}
		if (col + 2 <= 7 && row + 1 <= 7 && pos[row + 1][col + 2] == 0) {
			move.row2 = row + 1;
			move.col2 = col + 2;
			if (!willHaveCheck(move))
				return 0;
		}

		return 1;
		break;
	case wQUEEN:
	case bQUEEN:
		if (row != 0 && pos[row - 1][col] == 0) {
			move.row2 = row - 1;
			move.col2 = col;
			if (!willHaveCheck(move))
				return 0;
		}
		if (row != 7 && pos[row + 1][col] == 0 ) {
			move.row2 = row + 1;
			move.col2 = col;
			if (!willHaveCheck(move))
				return 0;
		}
		if (col != 0 && pos[row][col - 1] == 0) {
			move.row2 = row;
			move.col2 = col - 1;
			if (!willHaveCheck(move))
				return 0;
		}
		if (col != 7 && pos[row][col + 1] == 0) {
			move.row2 = row;
			move.col2 = col + 1;
			if (!willHaveCheck(move))
				return 0;
		}
		if (row - 1 >= 0 && col - 1 >= 0 && pos[row - 1][col - 1] == 0) {
			move.row2 = row - 1;
			move.col2 = col - 1;
			if (!willHaveCheck(move))
				return 0;
		}
		if (row + 1 <= 7 && col + 1 <= 7 && pos[row + 1][col + 1] == 0) {
			move.row2 = row + 1;
			move.col2 = col + 1;
			if (!willHaveCheck(move))
				return 0;
		}
		if (row - 1 >= 0 && col + 1 <= 7 && pos[row - 1][col + 1] == 0) {
			move.row2 = row - 1;
			move.col2 = col + 1;
			if (!willHaveCheck(move))
				return 0;
		}
		if (row - 1 >= 0 && col - 1 >= 0 && pos[row - 1][col - 1] == 0) {
			move.row2 = row - 1;
			move.col2 = col - 1;
			if (!willHaveCheck(move))
				return 0;
		}
		return 1;
		break;
	case wBISHOP:
	case bBISHOP:
		if (row - 1 >= 0 && col - 1 >= 0 && pos[row - 1][col - 1] == 0) {
			move.row2 = row - 1;
			move.col2 = col - 1;
			if (!willHaveCheck(move))
				return 0;
		}
		if (row + 1 <= 7 && col + 1 <= 7 && pos[row + 1][col + 1] == 0) {
			move.row2 = row + 1;
			move.col2 = col + 1;
			if (!willHaveCheck(move))
				return 0;
		}
		if (row - 1 >= 0 && col + 1 <= 7 && pos[row - 1][col + 1] == 0) {
			move.row2 = row - 1;
			move.col2 = col + 1;
			if (!willHaveCheck(move))
				return 0;
		}
		if (row - 1 >= 0 && col - 1 >= 0 && pos[row - 1][col - 1] == 0) {
			move.row2 = row - 1;
			move.col2 = col - 1;
			if (!willHaveCheck(move))
				return 0;
		}
		return 1;
		break;
	case wPAWN:
		if (row - 1 >= 0 && pos[row - 1][col] == 0) {
			move.row2 = row - 1;
			move.col2 = col;
			if (!willHaveCheck(move))
				return 0;
		}
		return 1;
		break;
	case bPAWN:
		if (row + 1 <= 7 && pos[row + 1][col] == 0) {
			move.row2 = row + 1;
			move.col2 = col;
			if (!willHaveCheck(move))
				return 0;
		}
		return 1;
		break;
	}
}