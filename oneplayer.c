#include "oneplayer.h"

extern int turn;
extern int pos[8][8];
extern int count;

int cpuPlay() {
	if (turn) {
		/*
			black
		*/
		int i, j, result = 1, toBreak = 0;

		//printf("count %d\n", count );

		if ((count / 2) % 2 == 0) {
			while (result) {
				int piece = rand() % 6;
				piece += 6;
				//printf("piece %d", piece);
				for (i = 0; i < 7; i++) {
					for (j = 0; j < 7; j++) {
						if (pos[i][j] == piece) {
							result = assignMove(piece, i, j);
							if (!result) {
								toBreak = 1;
								break;
							}
						}
					}
					if (toBreak)
						break;
				}
			}
		} else {
			while (result) {
				int piece = rand() % 6;
				piece += 6;
				//printf("piece %d", piece);
				for (i = 0; i < 7; i++) {
					for (j = 7; j > 0; j--) {
						if (pos[i][j] == piece) {
							result = assignMove(piece, i, j);
							if (!result) {
								toBreak = 1;
								break;
							}
						}
					}
					if (toBreak)
						break;
				}
			}

		}
	} else {
		/*
			white
		*/
		int i, j, result = 1, toBreak = 0;

		//printf("count %d\n", count );

		if ((count / 2) % 2 == 0) {
			while (result) {
				int piece = rand() % 5;
				piece ++;
				//printf("piece %d", piece);
				for (i = 7; i > 0; i--) {
					for (j = 7; j > 0; j--) {
						if (pos[i][j] == piece) {
							result = assignMove(piece, i, j);
							if (!result) {
								toBreak = 1;
								break;
							}
						}
					}
					if (toBreak)
						break;
				}
			}
		} else {
			while (result) {
				int piece = rand() % 5;
				piece ++;
				//printf("piece %d", piece);
				for (i = 7; i > 0; i--) {
					for (j = 0; j < 7; j++) {
						if (pos[i][j] == piece) {
							result = assignMove(piece, i, j);
							if (!result) {
								toBreak = 1;
								break;
							}
						}
					}
					if (toBreak)
						break;
				}
			}
		}
	}
	//18002086633
	//printBoard();
}

int assignMove(int piece, int row, int col) {
	Move move;
	move.piece = piece;
	move.row1 = row;
	move.col1 = col;
	switch (piece) {
	case wROOK:
	case bROOK:
		if (row != 0 && pos[row - 1][col] == 0) {
			move.row2 = row - 1;
			move.col2 = col;
			movePiece(move);
			return 0;
		} else if (row != 7 && pos[row + 1][col] == 0 ) {
			move.row2 = row + 1;
			move.col2 = col;
			movePiece(move);
			return 0;
		} else if (col != 0 && pos[row][col - 1] == 0) {
			move.row2 = row;
			move.col2 = col - 1;
			movePiece(move);
			return 0;
		} else if (col != 7 && pos[row][col + 1] == 0) {
			move.row2 = row;
			move.col2 = col + 1;
			movePiece(move);
			return 0;
		}
		return 1;
		break;
	case wKING:
	case bKING:
		if (row != 0 && pos[row - 1][col] == 0) {
			move.row2 = row - 1;
			move.col2 = col;
			movePiece(move);
			return 0;
		} else if (row != 7 && pos[row + 1][col] == 0 ) {
			move.row2 = row + 1;
			move.col2 = col;
			movePiece(move);
			return 0;
		} else if (col != 0 && pos[row][col - 1] == 0) {
			move.row2 = row;
			move.col2 = col - 1;
			movePiece(move);
			return 0;
		} else if (col != 7 && pos[row][col + 1] == 0) {
			move.row2 = row;
			move.col2 = col + 1;
			movePiece(move);
			return 0;
		} else if (row - 1 >= 0 && col - 1 >= 0 && pos[row - 1][col - 1] == 0) {
			move.row2 = row - 1;
			move.col2 = col - 1;
			movePiece(move);
			return 0;
		} else if (row + 1 <= 7 && col + 1 <= 7 && pos[row + 1][col + 1] == 0) {
			move.row2 = row + 1;
			move.col2 = col + 1;
			movePiece(move);
			return 0;
		} else if (row - 1 >= 0 && col + 1 <= 7 && pos[row - 1][col + 1] == 0) {
			move.row2 = row - 1;
			move.col2 = col + 1;
			movePiece(move);
			return 0;
		} else if (row - 1 >= 0 && col - 1 >= 0 && pos[row - 1][col - 1] == 0) {
			move.row2 = row - 1;
			move.col2 = col - 1;
			movePiece(move);
			return 0;
		}
		return 1;
		break;
	case wKNIGHT:
	case bKNIGHT:
		if (row - 2 >= 0 && col - 1 >= 0 && pos[row - 2][col - 1] == 0) {
			move.row2 = row - 2;
			move.col2 = col - 1;
			movePiece(move);
			return 0;
		} else if (row - 2 >= 0 && col + 1 <= 7 && pos[row - 2][col + 1] == 0) {
			move.row2 = row - 2;
			move.col2 = col + 1;
			movePiece(move);
			return 0;
		} else if (row + 2 <= 7 && col - 1 >= 0 && pos[row + 2][col - 1] == 0) {
			move.row2 = row + 2;
			move.col2 = col - 1;
			movePiece(move);
			return 0;
		} else if (row + 2 <= 7 && col + 1 <= 7 && pos[row + 2][col + 1] == 0) {
			move.row2 = row + 2;
			move.col2 = col + 1;
			movePiece(move);
			return 0;
		} else if (col - 2 >= 0 && row - 1 >= 0 && pos[row - 1][col - 2] == 0) {
			move.row2 = row - 1;
			move.col2 = col - 2;
			movePiece(move);
			return 0;
		} else if (col - 2 >= 0 && row + 1 <= 7 && pos[row + 1][col - 2] == 0) {
			move.row2 = row + 1;
			move.col2 = col - 2;
			movePiece(move);
			return 0;
		} else if (col + 2 <= 7 && row - 1 >= 0 && pos[row - 1][col + 2] == 0) {
			move.row2 = row - 1;
			move.col2 = col + 2;
			movePiece(move);
			return 0;
		} else if (col + 2 <= 7 && row + 1 <= 7 && pos[row + 1][col + 2] == 0) {
			move.row2 = row + 1;
			move.col2 = col + 2;
			movePiece(move);
			return 0;
		}

		return 1;
		break;
	case wQUEEN:
	case bQUEEN:
		if (row != 0 && pos[row - 1][col] == 0) {
			move.row2 = row - 1;
			move.col2 = col;
			movePiece(move);
			return 0;
		} else if (row != 7 && pos[row + 1][col] == 0 ) {
			move.row2 = row + 1;
			move.col2 = col;
			movePiece(move);
			return 0;
		} else if (col != 0 && pos[row][col - 1] == 0) {
			move.row2 = row;
			move.col2 = col - 1;
			movePiece(move);
			return 0;
		} else if (col != 7 && pos[row][col + 1] == 0) {
			move.row2 = row;
			move.col2 = col + 1;
			movePiece(move);
			return 0;
		} else if (row - 1 >= 0 && col - 1 >= 0 && pos[row - 1][col - 1] == 0) {
			move.row2 = row - 1;
			move.col2 = col - 1;
			movePiece(move);
			return 0;
		} else if (row + 1 <= 7 && col + 1 <= 7 && pos[row + 1][col + 1] == 0) {
			move.row2 = row + 1;
			move.col2 = col + 1;
			movePiece(move);
			return 0;
		} else if (row - 1 >= 0 && col + 1 <= 7 && pos[row - 1][col + 1] == 0) {
			move.row2 = row - 1;
			move.col2 = col + 1;
			movePiece(move);
			return 0;
		} else if (row - 1 >= 0 && col - 1 >= 0 && pos[row - 1][col - 1] == 0) {
			move.row2 = row - 1;
			move.col2 = col - 1;
			movePiece(move);
			return 0;
		}
		return 1;
		break;
	case wBISHOP:
	case bBISHOP:
		if (row - 1 >= 0 && col - 1 >= 0 && pos[row - 1][col - 1] == 0) {
			move.row2 = row - 1;
			move.col2 = col - 1;
			movePiece(move);
			return 0;
		} else if (row + 1 <= 7 && col + 1 <= 7 && pos[row + 1][col + 1] == 0) {
			move.row2 = row + 1;
			move.col2 = col + 1;
			movePiece(move);
			return 0;
		} else if (row - 1 >= 0 && col + 1 <= 7 && pos[row - 1][col + 1] == 0) {
			move.row2 = row - 1;
			move.col2 = col + 1;
			movePiece(move);
			return 0;
		} else if (row - 1 >= 0 && col - 1 >= 0 && pos[row - 1][col - 1] == 0) {
			move.row2 = row - 1;
			move.col2 = col - 1;
			movePiece(move);
			return 0;
		}
		return 1;
		break;
	case wPAWN:
		if (row - 1 >= 0 && pos[row - 1][col] == 0) {
			move.row2 = row - 1;
			move.col2 = col;
			movePiece(move);
			return 0;
		}
		return 1;
		break;
	case bPAWN:
		if (row + 1 <= 7 && pos[row + 1][col] == 0) {
			move.row2 = row + 1;
			move.col2 = col;
			movePiece(move);
			return 0;
		}
		return 1;
		break;
	}
}

