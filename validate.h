#ifndef VALIDATE

#define VALIDATE

#include <stdio.h>
#include "main.h"
#include "moves.h"

int validate(Move move);
int hasCheck(Move move);
int isPinned(Move move);
int isPieceAvailabe(Move move);
int isPositionAlreadyFilled(Move move);
int isMoveValidForPiece(Move move);
int isValidForBishop(Move move);
int isValidForRook(Move move);
int willHaveCheck(Move move);
int isCheckMate();
int hasPawnReachedEnd();
int isBlocked(int row, int col, int piece);
int canStopCheck(int piece, int row, int col);


#endif