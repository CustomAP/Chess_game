#include "main.h"

/*
	global variables
*/
int pos[8][8] = {0};


int turn = WHITE;
int count = 0;
int toEnd = 0;
int option;

int playerColor;

/*
	variables
*/
char name1[20], name2[20];
char name[20];

/*
	main function
*/
int main() {

	/*
		initial setup
	*/
	printf("*-----------------------------------------*Chess Game*-------------------------------------------*\n");

	printf("\n\nDescription:\n");

	printf("Made By Ashish Pawar\nNotation used: Long algebraic.\n(eg. e3-e4, Nb8-c6)\nEnter \"quit\" to end the game.\n\n\n");

	printf("Choose game:\n1.Single Player\n2.Two player\n");

	scanf("%d", &option);

	switch (option) {
	case 1:
		printf("\nSelect side\n1.Black\n2.White\n");
		scanf("%d", &playerColor);

		switch (playerColor) {
		case 1:
			playerColor = BLACK;
			break;
		case 2:
			playerColor = WHITE;
			break;
		}

		printf("\nEnter name for the Human player\n");
		scanf("%s", name);

		if (playerColor == WHITE) {
			strcpy(name1, name);
			strcpy(name2, "CPU");
		} else {
			strcpy(name2, name);
			strcpy(name1, "CPU");
		}

		break;
	case 2:
		printf("\nEnter player name for White\n");
		scanf("%s", name1);

		printf("\nEnter player name for Black\n");
		scanf("%s", name2);

		break;
	}

	/*
		initializing postions
	*/
	initPos();

	/*
		printing board in the initial state
	*/
	printBoard();


	while (1) {
		if (option == 2) {
			if (toEnd)
				break;
			if (playTwo()) {
				return 0;
			}
		}
		else {
			if (toEnd)
				break;
			if (playOne(playerColor)) {
				return 0;
			}
		}
	}
}


void printBoard() {
	int i, j;

	char indic = 'a';
	int indicNum = 1;

	printf("\n\n");

	for (i = 0; i < 46; i++)
		printf(" ");
	printf("%s\n", name2);


	for (i = 0; i < 46; i++)
		printf(" ");
	printf("BLACK\n");

	for (i = 0; i < 8; i++)
		printf("      %c     ", indic++);

	printf("\n\n");

	for (i = 0; i <= 32; i++) {
		printf("#  ");
	}
	//printf("\n");

	for (j = 0; j <= 32; j++) {
		for (i = 0; i <= 8; ++i)
		{

			if (j % 4 == 2) {
				if ((i == 0 || i == 8)) {
					printf("#     ");
					if (i != 8)
						printf("%c", piecePos((j - 2) / 4, i));
					else
						printf("%d", indicNum++);
					printf("     ");
				} else {

					printf("|     ");
					printf("%c", piecePos((j - 2) / 4, i));
					printf("     ");
				}
			} else if (j % 4 == 0 && j != 0 && j != 32) {
				if ((i == 0) ) {
					printf("-  ");
				} else {
					int k;
					for (k = 0; k < 4; k++)
						printf("-  ");
				}
			}
		}
		if (j != 31)
			printf("\n");
	}

	for (i = 0; i <= 32; i++) {
		printf("#  ");
	}
	printf("\n\n");

	for (i = 0; i < 46; i++)
		printf(" ");
	printf("WHITE\n");


	for (i = 0; i < 46; i++)
		printf(" ");
	printf("%s\n", name1);
}


void initPos() {
	int i;

	/*
		pawns
	*/
	for (i = 0; i < 8; i++) {
		pos[1][i] = bPAWN;
		pos[6][i] = wPAWN;
	}

	/*
		rooks
	*/
	pos[0][0] = bROOK;
	pos[0][7] = bROOK;
	pos[7][0] = wROOK;
	pos[7][7] = wROOK;

	/*
		knights
	*/
	pos[0][1] = bKNIGHT;
	pos[0][6] = bKNIGHT;
	pos[7][1] = wKNIGHT;
	pos[7][6] = wKNIGHT;

	/*
		bishops
	*/
	pos[0][2] = bBISHOP;
	pos[0][5] = bBISHOP;
	pos[7][2] = wBISHOP;
	pos[7][5] = wBISHOP;

	/*
		queens
	*/
	pos[0][3] = bQUEEN;
	pos[7][3] = wQUEEN;

	/*
		kings
	*/
	pos[0][4] = bKING;
	pos[7][4] = wKING;

}

char piecePos(int row, int col) {
	switch (pos[row][col]) {
	case bPAWN:
		return 'p';
		break;
	case bROOK:
		return 'r';
		break;
	case bKNIGHT:
		return 'n';
		break;
	case bBISHOP:
		return 'b';
		break;
	case bQUEEN:
		return 'q';
		break;
	case bKING:
		return 'k';
		break;
	case wPAWN:
		return 'P';
		break;
	case wROOK:
		return 'R';
		break;
	case wKNIGHT:
		return 'N';
		break;
	case wBISHOP:
		return 'B';
		break;
	case wQUEEN:
		return 'Q';
		break;
	case wKING:
		return 'K';
		break;
	default:
		return ' ';
		break;
	}
}


int playTwo() {
	char move[10];
	printf("\n%s>", turn ? "BLACK" : "WHITE");
	scanf("%s", move);
	if (strcmp(move, "quit") == 0)
		return 1;
	if (!interpretMove(move)) {
		count++;
		if (count % 2) {
			turn = BLACK;
		} else {
			turn = WHITE;
		}
	}

	return 0;
}

int playOne(int playerColor) {
	char move[10];
	if (turn == playerColor) {
		printf("\n%s>", turn ? "BLACK" : "WHITE");
		scanf("%s", move);
		if (strcmp(move, "quit") == 0
		   )
			return 1;
		if (!interpretMove(move)) {
			count++;
			if (count % 2) {
				turn = BLACK;
			} else {
				turn = WHITE;
			}
		}
	} else {
		cpuPlay();
		count++;
		if (count % 2) {
			turn = BLACK;
		} else {
			turn = WHITE;
		}
	}

	return 0;
}


void choosePiece() {
	int piece;
	int row, col, i;
	int valid = 1;

	if (!turn) {
		for (i = 0; i < 7; i++) {
			if (pos[0][i] == wPAWN) {
				row = 0;
				col = i;
				printf("%d %d\n", row, col );
				break;
			}
		}
	} else {
		for (i = 0; i < 7; i++) {
			if (pos[7][i] == bPAWN) {
				row = 7;
				col = i;

				printf("%d %d\n", row, col );
				break;
			}
		}
	}

	while (valid) {
		printf("Choose which piece to be replaced with the pawn\n1.Queen\n2.Rook\n3.Bishop\n4.Knight\n");
		scanf("%d", &piece);

		switch (piece) {
		case 1:
			if (!turn)
				pos[row][col] = wQUEEN;
			else
				pos[row][col] = bQUEEN;

			valid = 0;
			break;

		case 2:
			if (!turn)
				pos[row][col] = wROOK;
			else
				pos[row][col] = bROOK;

			valid = 0;
			break;

		case 3:
			if (!turn)
				pos[row][col] = wBISHOP;
			else
				pos[row][col] = bBISHOP;

			valid = 0;
			break;

		case 4:
			if (!turn)
				pos[row][col] = wKNIGHT;
			else
				pos[row][col] = bKNIGHT;

			valid = 0;
			break;

		default:
			printf("Invalid input\n");
			valid = 1;
			break;
		}

	}

}