#ifndef MOVES

#define MOVES

#include "main.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct Move{
	int piece;
	int row1,col1,row2,col2;
	int type;
}Move;

int interpretMove(char *move);
int movePiece(Move move);
int getCol(char col);


#include "validate.h"
#include "oneplayer.h"
#endif //MOVES DEFINED