project: main.o validate.o moves.o oneplayer.o
	cc  main.o validate.o moves.o oneplayer.o -o project -lm
main.o: main.o main.h validate.h moves.h oneplayer.h
	cc -c main.c
validate.o: validate.o main.h validate.h moves.h oneplayer.h   
	cc -c validate.c
moves.o: moves.o main.h validate.h moves.h oneplayer.h
	cc -c moves.c
oneplayer.o: oneplayer.o main.h validate.h moves.h oneplayer.h
	cc -c oneplayer.c
	