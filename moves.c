#include "moves.h"
#include "main.h"

extern int turn;
extern int pos[8][8];
extern int toEnd;
extern int option;
extern int playerColor;

int interpretMove(char *str) {
	// printf("interpretMove\n");
	Move move;
	Move move1, move2;
	if (strcmp(str, "0-0") == 0 || strcmp(str, "0-0-0") == 0) {
		/*
			For castling
		*/
		if (strcmp(str, "0-0") == 0) {

			/*
				Short castling
			*/

			if (!turn) {
				/*
					White
				*/
				if (pos[7][5] == 0 && pos[7][6] == 0 && pos[7][7] == wROOK && pos[7][4] == wKING) {

					/*
						move1 = rook
					*/
					move1.row1 = 7;
					move1.col1 = 7;

					move1.row2 = 7;
					move1.col2 = 5;

					move1.piece = wROOK;

					/*
						move2 = king;
					*/

					move2.row1 = 7;
					move2.col1 = 4;

					move2.row2 = 7;
					move2.col2 = 6;

					move2.piece = wKING;

					if (!hasCheck(move1) && !hasCheck(move2) && !willHaveCheck(move1) && !willHaveCheck(move2)) {
						/*
							if there is no check on both pieces and also in between then castle
						*/
						pos[7][6] = wKING;
						pos[7][5] = wROOK;

						pos[7][7] = 0;
						pos[7][4] = 0;
						system("clear");
						printBoard();
						return 0;
					} else {
						printf("Cannot castle\n");
						return 1;
					}
				} else {
					printf("Cannot castle\n");
					return 1;
				}
			} else {
				/*
					Black
				*/

				if (pos[0][5] == 0 && pos[0][6] == 0 && pos[0][7] == bROOK && pos[0][4] == bKING) {

					/*
						move1 = rook
					*/
					move1.row1 = 0;
					move1.col1 = 7;

					move1.row2 = 0;
					move1.col2 = 5;

					move1.piece = bROOK;

					/*
						move2 = king;
					*/

					move2.row1 = 0;
					move2.col1 = 4;

					move2.row2 = 0;
					move2.col2 = 6;

					move2.piece = bKING;

					if (!hasCheck(move1) && !hasCheck(move2) && !willHaveCheck(move1) && !willHaveCheck(move2)) {
						/*
							if there is no check on both pieces and also in between then castle
						*/
						pos[0][6] = bKING;
						pos[0][5] = bROOK;

						pos[0][7] = 0;
						pos[0][4] = 0;
						system("clear");
						printBoard();
						return 0;
					} else {
						printf("Cannot castle\n");
						return 1;
					}
				} else {
					printf("Cannot castle\n");
					return 1;
				}
			}

		} else {
			/*
				Long castling
			*/

			if (!turn) {
				/*
					White
				*/

				if (pos[7][1] == 0 && pos[7][1] == 0 && pos[7][3] == 0 && pos[7][0] == wROOK && pos[7][4] == wKING) {

					/*
						move1 = rook
					*/
					move1.row1 = 7;
					move1.col1 = 0;

					move1.row2 = 7;
					move1.col2 = 3;

					move1.piece = wROOK;

					/*
						move2 = king;
					*/

					move2.row1 = 7;
					move2.col1 = 4;

					move2.row2 = 7;
					move2.col2 = 2;

					move2.piece = wKING;

					if (!hasCheck(move1) && !hasCheck(move2) && !willHaveCheck(move1) && !willHaveCheck(move2)) {
						/*
							if there is no check on both pieces and also in between then castle
						*/
						pos[7][2] = wKING;
						pos[7][3] = wROOK;

						pos[7][0] = 0;
						pos[7][4] = 0;
						system("clear");
						printBoard();
						return 0;
					} else {
						printf("Cannot castle\n");
						return 1;
					}
				} else {
					printf("Cannot castle\n");
					return 1;
				}
			} else {
				/*
					Black
				*/
				if (pos[0][1] == 0 && pos[0][1] == 0 && pos[0][3] == 0 && pos[0][0] == bROOK && pos[0][4] == bKING) {

					/*
						move1 = rook
					*/
					move1.row1 = 0;
					move1.col1 = 0;

					move1.row2 = 0;
					move1.col2 = 3;

					move1.piece = bROOK;

					/*
						move2 = king;
					*/

					move2.row1 = 0;
					move2.col1 = 4;

					move2.row2 = 0;
					move2.col2 = 2;

					move2.piece = bKING;

					if (!hasCheck(move1) && !hasCheck(move2) && !willHaveCheck(move1) && !willHaveCheck(move2)) {
						/*
							if there is no check on both pieces and also in between then castle
						*/
						pos[0][2] = bKING;
						pos[0][3] = bROOK;

						pos[0][0] = 0;
						pos[0][4] = 0;
						system("clear");
						printBoard();
						return 0;
					} else {
						printf("Cannot castle\n");
						return 1;
					}
				} else {
					printf("Cannot castle\n");
					return 1;
				}
			}

		}

	} else if (str[0] >= 'A' && str[0] <= 'Z') {
		/*
			For higher pieces
		*/

		if (strlen(str) == 6) {
			switch (str[0]) {
			case 'N':
				/*
					Knight
				*/
				if (turn)
					move.piece = bKNIGHT;
				else
					move.piece = wKNIGHT;

				if (getCol(str[1]) == 10 || str[2] > '8' || str[2] < '1') {
					/*
						invalid input
					*/
					printf("Invalid Input\n");
					return 1;
				} else {
					move.col1 = getCol(str[1]);
					move.row1 = str[2] - '0' - 1;
					if (getCol(str[4]) == 10 || str[5] > '8' || str[5] < '1') {
						/*
							invalid input
						*/
						printf("Invalid Input\n");
						return 1;
					} else {
						move.row2 = str[5] - '0' - 1;
						move.col2 = getCol(str[4]);
					}
					if (str[3] == '-')
						move.type = PLAIN;
					else if (str[3] == 'x')
						move.type = CAPTURE;
					else {
						/*
							invalid input
						*/
						printf("Invalid Input\n");
						return 1;
					}
				}
				if (movePiece(move))
					return 1;
				return 0;
				break;
			case 'B':
				/*
					Bishop
				*/
				if (turn)
					move.piece = bBISHOP;
				else
					move.piece = wBISHOP;

				if (getCol(str[1]) == 10 || str[2] > '8' || str[2] < '1') {
					/*
						invalid input
					*/
					printf("Invalid Input\n");
					return 1;
				} else {
					move.col1 = getCol(str[1]);
					move.row1 = str[2] - '0' - 1;
					if (getCol(str[4]) == 10 || str[5] > '8' || str[5] < '1') {
						printf("Here\n");
						/*
							invalid input
						*/
						printf("Invalid Input\n");
						return 1;
					} else {
						move.row2 = str[5] - '0' - 1;
						move.col2 = getCol(str[4]);
					}
					if (str[3] == '-')
						move.type = PLAIN;
					else if (str[3] == 'x')
						move.type = CAPTURE;
					else {
						/*
							invalid input
						*/
						printf("Invalid Input\n");
						return 1;
					}
				}
				if (movePiece(move))
					return 1;
				return 0;
				break;
			case 'R':
				/*
					Rook
				*/
				if (turn)
					move.piece = bROOK;
				else
					move.piece = wROOK;

				if (getCol(str[1]) == 10 || str[2] > '8' || str[2] < '1') {
					/*
						invalid input
					*/
					printf("Invalid Input\n");
					return 1;
				} else {
					move.col1 = getCol(str[1]);
					move.row1 = str[2] - '0' - 1;
					if (getCol(str[4]) == 10 || str[5] > '8' || str[5] < '1') {
						/*
							invalid input
						*/
						printf("Invalid Input\n");
						return 1;
					} else {
						move.row2 = str[5] - '0' - 1;
						move.col2 = getCol(str[4]);
					}
					if (str[3] == '-')
						move.type = PLAIN;
					else if (str[3] == 'x')
						move.type = CAPTURE;
					else {
						/*
							invalid input
						*/
						printf("Invalid Input\n");
						return 1;
					}
				}
				if (movePiece(move))
					return 1;
				return 0;
				break;
			case 'Q':
				/*
					Queen
				*/
				if (turn)
					move.piece = bQUEEN;
				else
					move.piece = wQUEEN;

				if (getCol(str[1]) == 10 || str[2] > '8' || str[2] < '1') {
					/*
						invalid input
					*/
					printf("Invalid Input\n");
					return 1;
				} else {
					move.col1 = getCol(str[1]);
					move.row1 = str[2] - '0' - 1;
					if (getCol(str[4]) == 10 || str[5] > '8' || str[5] < '1') {
						/*
							invalid input
						*/
						printf("Invalid Input\n");
						return 1;
					} else {
						move.row2 = str[5] - '0' - 1;
						move.col2 = getCol(str[4]);
					}
					if (str[3] == '-')
						move.type = PLAIN;
					else if (str[3] == 'x')
						move.type = CAPTURE;
					else {
						/*
							invalid input
						*/
						printf("Invalid Input\n");
						return 1;
					}
				}
				if (movePiece(move))
					return 1;
				return 0;
				break;
			case 'K':
				/*
					King
				*/
				if (turn)
					move.piece = bKING;
				else
					move.piece = wKING;

				if (getCol(str[1]) == 10 || str[2] > '8' || str[2] < '1') {
					/*
						invalid input
					*/
					printf("Invalid Input\n");
					return 1;
				} else {
					move.col1 = getCol(str[1]);
					move.row1 = str[2] - '0' - 1;
					if (getCol(str[4]) == 10 || str[5] > '8' || str[5] < '1') {
						/*
							invalid input
						*/
						return 1;
					} else {
						move.row2 = str[5] - '0' - 1;
						move.col2 = getCol(str[4]);
					}
					if (str[3] == '-')
						move.type = PLAIN;
					else if (str[3] == 'x')
						move.type = CAPTURE;
					else {
						/*
							invalid input
						*/
						printf("Invalid Input\n");
						return 1;
					}
				}
				if (movePiece(move))
					return 1;
				return 0;
				break;
			}
		} else {
			/*
				invalid input
			*/
			printf("Invalid Input\n");
			return 1;
		}
	} else {

		/*
			For Pawns
		*/
		if (turn)
			move.piece = bPAWN;
		else
			move.piece = wPAWN;

		if (strlen(str) == 5) {
			if (getCol(str[0]) == 10 || str[1] > '8' || str[1] < '1') {
				/*
					invalid input
				*/
				printf("Invalid Input\n");
				return 1;
			} else {
				move.col1 = getCol(str[0]);
				move.row1 = str[1] - '0' - 1;
				if (getCol(str[3]) == 10 || str[4] > '8' || str[4] < '1') {
					/*
						invalid input
					*/
					printf("Invalid Input\n");
					return 1;
				} else {
					move.col2 = getCol(str[3]);
					move.row2 = str[4] - '0' - 1;
				}
				if (str[2] == '-')
					move.type = PLAIN;
				else if (str[2] == 'x')
					move.type = CAPTURE;
				else {
					/*
						invalid input
					*/
					printf("Invalid Input\n");
					return 1;
				}
			}
			if (movePiece(move))
				return 1;
			return 0;
		} else {
			/*
				invalid input
			*/
			printf("Invalid Input\n");
			return 1;
		}
	}
}


int movePiece(Move move) {
	if (validate(move)) {
		/*
			invalid move
		*/
		if (option == 2 || (option == 1 && playerColor == turn))
			printf("Invalid Move\n");
		return 1;

	} else if (hasCheck(move) && isCheckMate()) {
		/*
			Check mate
		*/
		printf("Its a CheckMate!! :)\n");
		toEnd = 1;
		return 1;
	} else if (hasCheck(move)) {
		/*
			check
		*/
		// printf("move %d %d %d\n",move.piece,move.row2,move.col2 );
		// printf("here\n");
		return 1;
	} else if (isPinned(move)) {
		/*
			pin
		*/
		return 1;
	} else {
		/*
			valid move
		*/
		pos[move.row1][move.col1] = 0;
		pos[move.row2][move.col2] = move.piece;
		system("clear");
		printBoard();

		if (hasPawnReachedEnd()) {
			printf("Pawn reached the end\n");
			choosePiece();
			system("clear");
			printBoard();
		}

		return 0;
	}
}

int getCol(char col) {
	switch (col) {
	case 'a':
		return 0;
		break;
	case 'b':
		return 1;
		break;
	case 'c':
		return 2;
		break;
	case 'd':
		return 3;
		break;
	case 'e':
		return 4;
		break;
	case 'f':
		return 5;
		break;
	case 'g':
		return 6;
		break;
	case 'h':
		return 7;
		break;
	default:
		/*
			invalid input
		*/
		return 10;
		break;
	}
}
