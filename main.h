#ifndef MAIN

#define MAIN

#include <stdio.h>
#include "moves.h"

void printBoard();
void initPos();
char piecePos(int row, int col);
int playTwo();
int playOne(int color);
void choosePiece();

/*
	structures
*/
typedef struct piece {
	int piece, row, col;
} piece;


/*
	enum for pieces
*/
enum pieces {wPAWN = 1, wROOK = 4, wKNIGHT = 2, wBISHOP = 3, wQUEEN = 5, wKING = 6, bPAWN = 7, bROOK = 9, bKNIGHT = 8, bBISHOP = 10, bQUEEN = 11, bKING = 12};

enum turns {WHITE, BLACK};

enum movetype {PLAIN, CAPTURE};

#include "oneplayer.h"
	
#endif //MAIN DEFINED