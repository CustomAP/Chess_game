#ifndef ONEPLAYER

#define ONEPLAYER

#include "main.h"
#include "moves.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int cpuPlay();
int assignMove(int piece, int row, int col);

#endif //ONEPLAYER DEFINED
