Title : Chess Game

MIS : 111608051

Description:

	This chess game consits of two modes:
		1.Single Player
		2.Two Player

This game follows all rules of chess game. Every condition like check, illegal moves, piece pin, checkmate, piece capture, long castling, short castling, piece conversion from pawn, etc. is taken care of.

The game uses terminal to print the board and play the game.

Moves are entered in "Long-algebraic" notation of chess moves.
	eg. e2-e3, Ng8-f6, 0-0, 0-0-0.

In One player mode, the computer calculates one and maximum two further moves in case of check.

This is an attempt to create a chess game with a basic level CPU player and a robust two-player chess game.

The game ends when one of the player is checkmate. However "quit" command can be used to end the game in between.